package com.ym.hospitaljsonparser.modals

import com.google.gson.annotations.SerializedName

data class Nurse(
        @SerializedName("first_name") val first_name: String,
        @SerializedName("family_name") val family_name: String,
        @SerializedName("ward_number") val ward_number: Int,
        @SerializedName("staff_uuid") val staff_uuid: String
)