package com.ym.hospitaljsonparser.modals

import com.google.gson.annotations.SerializedName

data class Patient(
        @SerializedName("first_name") val first_name: String,
        @SerializedName("family_name") val family_name: String,
        @SerializedName("profession") val profession: String
)