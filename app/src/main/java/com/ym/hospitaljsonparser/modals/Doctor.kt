package com.ym.hospitaljsonparser.modals

import com.google.gson.annotations.SerializedName

data class Doctor(
        @SerializedName("first_name") val first_name: String,
        @SerializedName("family_name") val family_name: String,
        @SerializedName("pager_number") val pager_number: String,
        @SerializedName("staff_uuid") val staff_uuid: String
)