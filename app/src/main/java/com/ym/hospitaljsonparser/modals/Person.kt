package com.ym.hospitaljsonparser.modals

import com.google.gson.annotations.SerializedName

data class Person(
        @SerializedName("first_name") val first_name: String,
        @SerializedName("family_name") val family_name: String,
        @SerializedName("profession") val profession: String,
        @SerializedName("ward_number") val ward_number: Int,
        @SerializedName("staff_uuid") val staff_uuid: String,
        @SerializedName("pager_number") val pager_number: String,
        @SerializedName("class") val cls: String
)