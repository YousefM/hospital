package com.ym.hospitaljsonparser

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import com.ym.hospitaljsonparser.modals.Doctor
import com.ym.hospitaljsonparser.modals.Nurse
import com.ym.hospitaljsonparser.modals.Patient
import com.ym.hospitaljsonparser.modals.Person
import com.ym.hospitaljsonparser.utilities.FileUtilities
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.util.*


class MainActivity : AppCompatActivity() {

    private val CHOICE_JSON_FILE_REQUEST_CODE = 1798
    private val READ_STORAGE_PERMISSION_REQUEST_CODE = 1799

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_input.setOnClickListener a@{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val result = checkSelfPermission(READ_EXTERNAL_STORAGE)

                if (result != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            this, arrayOf(READ_EXTERNAL_STORAGE),
                            READ_STORAGE_PERMISSION_REQUEST_CODE
                    )
                    return@a
                }
            }

            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
            val mime = "text/json"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mime)
            intent.type = mime
            startActivityForResult(Intent.createChooser(intent, "asfsf"), CHOICE_JSON_FILE_REQUEST_CODE)
        }

        btn_parse.setOnClickListener {
            try {
                val persons = Gson().fromJson(edt_input.text.toString(), Array<Person>::class.java)

                val nurses = ArrayList<Nurse>()
                val doctors = ArrayList<Doctor>()
                val patients = ArrayList<Patient>()

                for (person in persons)
                    when (person.cls) {
                        "Patient" -> patients.add(Patient(person.first_name, person.family_name, person.profession))
                        "Nurse" -> nurses.add(
                                Nurse(
                                        person.first_name,
                                        person.family_name,
                                        person.ward_number,
                                        person.staff_uuid
                                )
                        )
                        "Doctor" -> doctors.add(
                                Doctor(
                                        person.first_name,
                                        person.family_name,
                                        person.pager_number,
                                        person.staff_uuid
                                )
                        )
                    }

                val stringBuilder = StringBuilder()

                stringBuilder.append("Patients\n")
                patients.sortBy { patient -> patient.first_name }
                patients.sortBy { patient -> patient.family_name }
                for ((first_name, family_name) in patients)
                    stringBuilder.append("$first_name $family_name\n")


                stringBuilder.append("\nNurses\n")
                nurses.sortBy { patient -> patient.first_name }
                nurses.sortBy { patient -> patient.family_name }
                nurses.sortBy { patient -> patient.ward_number }
                val nurseGroups = nurses.groupBy { nurse -> nurse.ward_number }
                nurseGroups.forEach {
                    stringBuilder.append("[Ward ${it.key}]\n")
                    for ((first_name, family_name) in it.value)
                        stringBuilder.append("$first_name $family_name\n")
                }

                stringBuilder.append("\nDoctors\n")
                doctors.sortBy { patient -> patient.first_name }
                doctors.sortBy { patient -> patient.family_name }
                for ((first_name, family_name, pager_number) in doctors)
                    stringBuilder.append("$first_name $family_name: $pager_number\n")

                edt_input.setText(stringBuilder.toString())
            } catch (ex: Exception) {
                Toast.makeText(this, "Failed to parse data", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK || requestCode != CHOICE_JSON_FILE_REQUEST_CODE) return
        if (data == null || data.data == null) return

        try {
            edt_input.setText(File(FileUtilities.getPath(this, data.data!!)!!).readText())
        } catch (ex: Exception) {
            Toast.makeText(this, "Failed to open file", Toast.LENGTH_SHORT).show()
        }
    }
}
